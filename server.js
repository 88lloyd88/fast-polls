const Discord = require('discord.js');
const {Collection} = require('discord.js');
const client = new Discord.Client();

const token = 'Njg4NTQ0MTYxMjA4NDY3NDgz.Xm127A.5QChAg8mT1Z_DRwCLwZ0KQi9pEU';

const prefix = '/';

const emojiAlphabet = [
    ['🇦',0],
    ['🇧',0],
    ['🇨',0],
    ['🇩',0],
    ['🇪',0],
    ['🇫',0],
    ['🇬',0],
    ['🇭',0],
    ['🇮',0],
    ['🇯',0],
    ['🇰',0],
    ['🇱',0],
    ['🇲',0],
    ['🇳',0],
    ['🇴',0],
    ['🇵',0],
    ['🇶',0],
    ['🇷',0],
    ['🇸',0],
    ['🇹',0],
    ['🇺',0],
    ['🇻',0],
    ['🇼',0],
    ['🇽',0],
    ['🇾',0],
    ['🇿',0]
];

// the messages that users can only react 3 times with
const polls = new Set()
// Collection<Message, Collection<User, number>>: stores how many times a user has reacted on a message
const reactionCount = new Collection()

const reactions = [];
let updOptions = [];
var question;
var limit = 1;
var Embed;

function hasOptions(element){
    return element == '-o';
}

client.on('ready', () => {
    console.log('This client is online!');
})

client.on('message', message=>{
try{    
    let args = message.content.substring(prefix.length).split(" ");
    switch(args[0]){
        case "poll":
            Embed = new Discord.MessageEmbed()
                .setColor(0xFFC300)
                .setTitle("Initiate Poll")
                .setDescription('/poll\n -q insert question here\n -o Insert, options, here\n -l optional max number of answers');

            if(args[1] != '-q' || args.find(hasOptions) != '-o'){
                message.channel.send(Embed);
                break;
            }

            let msgArgs = args.slice(1).join(" ").split("-q");
            let questionArgs = msgArgs.slice(1).toString().split("-o");
            let optionLimit = questionArgs.slice(1).toString().split("-l");
            let options = optionLimit[0].slice(0).toString().split(",");
            question = "**"+questionArgs[0].trim()+"**";

            const limitParam = Number(optionLimit.slice(1).join("").trim());
            if (limitParam > 1) {
                limit = limitParam;
            }
            
            var optionsList = '';
            for (var i = 0; i < options.length; i++){
                optionsList = optionsList.concat("\n",emojiAlphabet[i][0],"-",options[i].trim()," **",emojiAlphabet[i][1]," Votes**");
                reactions[i] = emojiAlphabet[i][0];
                updOptions[emojiAlphabet[i][0]] = ["\n".concat(emojiAlphabet[i][0],"-",options[i].trim()," **"), emojiAlphabet[i][1]];
            } 

            Embed = new Discord.MessageEmbed()
                .setColor(0x6666ff)
                .setTitle(question)
                .setDescription(optionsList);

                message.channel.send(Embed).then(messageReaction => {
                
                    // when you send a poll add the message the client sent to the set:
                    polls.add(messageReaction);

                    for (var i = 0; i < options.length; i++){
                        messageReaction.react(emojiAlphabet[i][0]);
                    }
                
                    message.delete().catch(console.error);
                });          
        }
    }catch{
        message.channel.send("Error");
    }  
});

client.on('messageReactionAdd', (reaction, user) => {
    try{
        // edit: so that this does not run when the client reacts
        if (user.id === client.user.id) return
        //if(!isNaN(limit) && limit>0){
        const {message} = reaction;
        
        // only do the following if the message is one of the polls
        if (polls.has(message)) {
            // if message hasn't been added to collection add it
            if (!reactionCount.get(message)) reactionCount.set(message, new Collection())
            // reaction counts for this message
            const userCount = reactionCount.get(message)
            if(reactions.indexOf(reaction.emoji.name)>-1){
                // add 1 to the user's reaction count
                userCount.set(user, (userCount.get(user) || 0) + 1)
                updOptions[reaction.emoji.name][1]++;
                if (userCount.get(user) > limit) {
                    reaction.users.remove(user)
                    // <@!id> mentions the user (using their nickname if they have one)
                    message.channel.send(`<@!${user.id}>, you've already voted `.concat(limit,` times, please remove a reaction to vote again.`))
                } else {
                    var descr = '';
                    for (var element in updOptions) {
                        descr = descr.concat(updOptions[element][0],updOptions[element][1].toString()," Votes**");
                    };

                    Embed = new Discord.MessageEmbed()
                        .setColor(0x6666ff)
                        .setTitle(question)
                        .setDescription(descr);

                    // update message
                    message.edit(Embed);

                    
                }
            }
        }
    }catch{
        message.channel.send("update error");
    }
});

client.on('messageReactionRemove', (reaction, user) => {
    // edit: so that this does not run when the bot reacts
    try{
        if (user.id === client.user.id) return
  
        const {message} = reaction
        const userCount = reactionCount.get(message)
        // subtract 1 from user's reaction count
        if (polls.has(message)) userCount.set(user, reactionCount.get(message).get(user) - 1)

        updOptions[reaction.emoji.name][1]--;
        var descr = '';
        for (var element in updOptions) {
            descr = descr.concat(updOptions[element][0],updOptions[element][1].toString()," Votes**");
        };
    
    

        Embed = new Discord.MessageEmbed()
            .setColor(0x6666ff)
            .setTitle(question)
            .setDescription(descr);

        // update message
        message.edit(Embed);
        }catch{
            message.channel.send("refresh error");
    }
});

client.login(token);